<?php
function get_links($link)
{
    // хранение результатов
    $ret = array();

    // создаем новый объект класса
    $dom = new domDocument;

    // получаем контент
    @$dom->loadHTML(file_get_contents($link));

    // убираем пробелы
    $dom->preserveWhiteSpace = false;

    // извлекаем все теги ссылок
    $links = $dom->getElementsByTagName('a');

    // получаем значение артибута href для всех ссылок
    foreach ($links as $tag) {
        $ret[$tag->getAttribute('href')] = $tag->childNodes->item(0)->nodeValue;
    }
    return $ret;
}

// ссылка для сбора ссылок
$link = "https://publicaccess.westoxon.gov.uk/online-applications/pagedSearchResults.do";
// в функцию пишим либо ссылку $link, либо файл (так как иногда по ссылке не получить код страницы, как на примере сслке выше)
//поэтому нужно загрузить стр. в браузере и вставить код в file.php
$urls = get_links('file.php');

// проверяем
if (sizeof($urls) > 0) {
    // выводим
    foreach ($urls as $key => $value) {

        echo 'http://public.oxford.gov.uk' . $key . '<br >';
    }
} else {
    echo "Не удалось получить ссылки на странице $link";
}