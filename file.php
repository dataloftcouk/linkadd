

























<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>

    <!-- #BeginEditable "doctitle" --><title>Search Results</title><!-- #EndEditable -->
    <!-- #BeginEditable "metatags" --><!-- #EndEditable -->

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- Customer metatags -->


    <!-- Favicon link -->
    <link rel="icon" href="http://public.oxford.gov.uk/online-applications-skin/images/oxford/favicon.ico" type="image/x-icon" />

    <link rel="stylesheet" href="http://public.oxford.gov.uk/online-applications-skin/styles/screen.css" type="text/css" media="screen,projection" />
    <link rel="stylesheet" href="http://public.oxford.gov.uk/online-applications-skin/styles/theme1.css" type="text/css"/>
    <link rel="stylesheet" href="http://public.oxford.gov.uk/online-applications-skin/styles/print.css" type="text/css" media="print" />

    <script type="text/javascript" src="http://public.oxford.gov.uk/online-applications-skin/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="http://public.oxford.gov.uk/online-applications-skin/scripts/jquery.toolbar.idox.min.js"></script>
    <script type="text/javascript" src="http://public.oxford.gov.uk/online-applications-skin/scripts/common.js"></script>
    <script type="text/javascript" src="http://public.oxford.gov.uk/online-applications-skin/scripts/checkbox.js"></script>
    <script type="text/javascript" src="http://public.oxford.gov.uk/online-applications-skin/scripts/amplify.min.js"></script>
    <script type="text/javascript" src="http://public.oxford.gov.uk/online-applications-skin/scripts/createCommentCookie.js"></script>

    <!-- Customer Analytics Script -->


    <script type="text/javascript">
        $('html').addClass('js');
        $(document).ready(function(){
            $('.main#apply>span:first-child').attr('tabindex', '0')
        });
    </script>

    <!-- #BeginEditable "mobile" --><!-- #EndEditable -->
    <!-- #BeginEditable "head" --><!-- #EndEditable -->
    <!-- #BeginEditable "webapp" -->
    <script type="text/javascript">
        $(document).ready( function() {
            var form = document.getElementById("mapForm");
            form.submit();
        });
    </script>
    <!-- #EndEditable -->

</head>
<body>

<a href="#pageheading" class="sr-only">Skip to main content</a>

<!-- IDOX PA START -->
<div id="idox">
    <div id="pa">
        <div id="header">
            <div class="container">
                <div id="logo"><a href="https://www.oxford.gov.uk/" title="Oxford City Council home page"><img src="http://public.oxford.gov.uk/online-applications-skin/images/oxford/logo.png" alt="Council logo" /><span class="sr-only">Oxford City Council</span></a></div>
                <div id="headertitle"><a href="https://www.oxford.gov.uk/"><span class="site-header__logo-heading">Oxford City Council</span><span class="strapline">Building a world class city for everyone</span></a></div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="https://www.oxford.gov.uk/">Home</a></li><li><span>/</span><a href="https://www.oxford.gov.uk/planning">Planning</a></li><li><span>/</span><a href="https://www.oxford.gov.uk/info/20066/planning_applications">Planning Applications</a></li><li><span>/</span>View and comment on planning applications</li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
        <div class="container">
            <div id="toolbar" class="nojs">
                <ul>
                    <li id="search" class="main"><span tabindex="0">Search&nbsp;</span><span class="caret"></span>
                        <ul>
                            <!-- #BeginLibraryItem "/Library/searchModulesNavItems.lbi" -->















                            <li id="planLinks">
                                <a href="/online-applications/search.do?action=simple&amp;searchType=Application" >
                                    <span>Planning</span>
                                </a>
                                <ul>
                                    <li id="planBasicSearch">
                                        <a href="/online-applications/search.do?action=simple&amp;searchType=Application">
                                            Simple Search
                                        </a>
                                    </li>

                                    <li id="planAdvancedSearch">
                                        <a href="/online-applications/search.do?action=advanced&amp;searchType=Application">
                                            Advanced
                                        </a>
                                    </li>
                                    <li id="planListSearch">
                                        <a href="/online-applications/search.do?action=weeklyList&amp;searchType=Application">
                                            Weekly/Monthly Lists
                                        </a>
                                    </li>
                                    <li id="planPropertySearch">
                                        <a href="/online-applications/search.do?action=property&amp;type=custom">
                                            Property Search
                                        </a>
                                    </li>


                                    <li id="planMapsearch">
                                        <a href="/online-applications/spatialDisplay.do?action=display&amp;searchType=Application">
                                            Map Search
                                        </a>
                                    </li>


                                </ul>
                            </li>



                            <li id="bcLinks">
                                <a href="/online-applications/search.do?action=simple&amp;searchType=BuildingControl">
                                    <span>Building Control</span>
                                </a>
                                <ul>
                                    <li id="bcBasicSearch">
                                        <a href="/online-applications/search.do?action=simple&amp;searchType=BuildingControl">
                                            Simple Search
                                        </a>
                                    </li>
                                    <li id="bcAdvancedSearch">
                                        <a href="/online-applications/search.do?action=advanced&amp;searchType=BuildingControl">
                                            Advanced
                                        </a>
                                    </li>
                                    <li id="bcListSearch">
                                        <a href="/online-applications/search.do?action=weeklyList&amp;searchType=BuildingControl">
                                            Weekly/Monthly Lists
                                        </a>
                                    </li>
                                    <li id="bcPropertySearch">
                                        <a href="/online-applications/search.do?action=property&amp;type=custom">
                                            Property Search
                                        </a>
                                    </li>


                                    <li id="bcMapsearch">
                                        <a href="/online-applications/spatialDisplay.do?action=display&amp;searchType=BuildingControl">
                                            Map Search
                                        </a>
                                    </li>


                                </ul>
                            </li>





                            <li id="liLinks">
                                <a href="/online-applications/search.do?action=simple&amp;searchType=LicencingApplication">
                                    <span>Licensing</span>
                                </a>

                                <ul>
                                    <li id="liBasicSearch">
                                        <a href="/online-applications/search.do?action=simple&amp;searchType=LicencingApplication">
                                            Simple Search
                                        </a>
                                    </li>
                                    <li id="liAdvancedSearch">
                                        <a href="/online-applications/search.do?action=advanced&amp;searchType=LicencingApplication">
                                            Advanced
                                        </a>
                                    </li>
                                    <li id="liPropertySearch">
                                        <a href="/online-applications/search.do?action=property&amp;type=custom">
                                            Property Search
                                        </a>
                                    </li>


                                    <li id="liMapsearch">
                                        <a href="/online-applications/spatialDisplay.do?action=display&amp;searchType=LicencingApplication">
                                            Map Search
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <!-- #EndLibraryItem -->
                        </ul>
                    </li>
                    <!-- #BeginEditable "applicationForms" -->






                    <!-- #EndEditable -->
                    <li id="myprofile" class="main"><span tabindex="0">My Profile&nbsp;</span><span class="caret"></span>
                        <ul>
                            <!-- #BeginLibraryItem "/Library/consulteeInTrayNavItem.lbi" -->






                            <!-- #EndLibraryItem -->
                            <li><a href="http://public.oxford.gov.uk/online-applications/registered/displayUserDetails.do">Profile Details</a></li>
                            <li><a href="http://public.oxford.gov.uk/online-applications/registered/savedSearch.do?action=display">Saved Searches</a></li>
                            <li><a href="http://public.oxford.gov.uk/online-applications/registered/userAdmin.do?action=showNotifiedApplications">Notified Applications</a></li>
                            <li><a href="http://public.oxford.gov.uk/online-applications/registered/trackedApplication.do?action=display">Tracked Applications</a></li>
                            <!-- #BeginEditable "formSubmissions" -->








                            <!-- #EndEditable -->
                        </ul>
                    </li>
                    <li id="loginLink" class="main"><!-- #BeginEditable "loginlogout" -->










                        <a href="/online-applications/registered/userAdmin.do">Login</a>
                        <!-- #EndEditable --></li>
                    <!-- #BeginEditable "register" -->











                    <li id="register">
                        <a href="/online-applications/registrationWizard.do?action=start">Register</a>
                    </li>

                    <!-- #EndEditable -->
                    <!-- #BeginLibraryItem "/Library/applyOnlineNavItems.lbi" -->





                    <!-- #EndLibraryItem -->
                </ul>
            </div>

            <!-- #BeginEditable "headlinenews" --><!-- #EndEditable -->

            <div id="pageheading">
                <h1><strong>Planning</strong>&nbsp;&ndash;&nbsp;<!-- #BeginEditable "pageheading" -->










                    Results for Application Search




                    <!-- #EndEditable --></h1>
                <!-- #BeginEditable "helplink" --><!-- #EndEditable -->
            </div>

            <!-- START WAM CONTENT -->
            <div class="content">
                <!-- #BeginEditable "errormsg" --><!-- #EndEditable -->
                <!-- #BeginEditable "pagehead" --><!-- #EndEditable -->
                <!-- #BeginEditable "tabs" --><!-- #EndEditable -->
                <!-- #BeginEditable "tabsubnav" --><!-- #EndEditable -->
                <!-- #BeginEditable "pagebody" -->





                <div id="searchtools">
                    <ul>



                        <li>
                            <a href="/online-applications/refineSearch.do?action=refine" id="refinesearch"><img src="http://public.oxford.gov.uk/online-applications-skin/images/button_refinesearch.gif" alt="Refine search icon" /></a>
                        </li>


                        <li>
                            <a href="/online-applications/registered/saveSearch.do?action=saveDialog" id="savesearch"><img src="http://public.oxford.gov.uk/online-applications-skin/images/button_savesearch.gif" alt="Save search icon" /></a>
                        </li>





                        <li>
                            <a href="/online-applications/pagedSearchResults.do?action=printPreview" rel="external" id="print" title="View a printable version of these search results (opens in a new window)">
                                <img src="http://public.oxford.gov.uk/online-applications-skin/images/button_print.gif" alt="Print icon"/>
                            </a>
                        </li>

                    </ul>
                </div>




                <div id="searchfilters">

                    <form name="searchCriteriaForm" method="post" action="/online-applications/pagedSearchResults.do" id="searchResults">
                        <input type="hidden" name="searchCriteria.page" value="1" />
                        <input type="hidden" name="action" value="page" />


                        <span class="orderByContainer">
                        <label for="orderBy">
                            Sort by
                        </label>
                        <select name="orderBy" id="orderBy"><option value="Reference">Ref. No.</option>

                                <option value="DateReceived" selected="selected">Date Received</option>

                                <option value="Proposal">Description</option>

                                <option value="ExpiryDate">Expiry Date</option></select>
                    </span>


                        <span class="orderByDirectionContainer">
                    <label for="orderByDirection">
                        Direction
                    </label>
                    <select name="orderByDirection" id="orderByDirection"><option value="Ascending">Ascending</option>

                            <option value="Descending" selected="selected">Descending</option></select>
				</span>

                        <span class="resultsPerPage">
                    <label for="resultsPerPage">
                        Results per page
                    </label>
                    <select name="searchCriteria.resultsPerPage" id="resultsPerPage"><option value="5">5</option>
<option value="10">10</option>
<option value="20">20</option>
<option value="50">50</option>
<option value="100" selected="selected">100</option></select>
				</span>
                        <input type="submit" value="Go" class="button primary" />
                    </form>
                </div>




                <div id="searchResultsContainer" class="panel">







                    <p class="pager top"><span class="showing"><strong>Showing 801-816</strong> of 816</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=8" class="previous"><img border="0" src="http://public.oxford.gov.uk/online-applications-skin/images/pager_previous.gif" alt="" title="Display the previous page of search results"/>Previous</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=1" class="page">1</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=2" class="page">2</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=3" class="page">3</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=4" class="page">4</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=5" class="page">5</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=6" class="page">6</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=7" class="page">7</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=8" class="page">8</a><span class="divider">|</span><strong>9</strong><span class="divider">|</span></p>


                    <div class="col-a">

                        <ul id="searchresults">



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P1BEA8MF11T00&amp;activeTab=summary">
                                    Erection of a single storey rear extension.
                                </a>









                                <p class="address">


                                    34 Mere Road Oxford Oxfordshire OX2 8AN
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03416/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 21 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 05 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P1BDNTMFFW200&amp;activeTab=summary">
                                    Demolition of existing building. Erection of single storey building for use as a laundry (B1(c))
                                </a>









                                <p class="address">


                                    38 Old Marston Road Oxford OX3 0JP
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03412/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 21 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 03 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P199XPMF0XV00&amp;activeTab=summary">
                                    Erection of a single storey side orangery extension and formation of 1no window to side elevation.
                                </a>









                                <p class="address">


                                    19 Harberton Mead Oxford Oxfordshire OX3 0DB
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03388/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 20 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 02 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P19IZZMFFV200&amp;activeTab=summary">
                                    Change of Use of Farm and Outbuildings to B1(a) office use for a Design Studio, Erection of an additional building to provide further B1(a) office space and a create of a 1 x 2-bed dwellinghouse (Use Class C3), alterations to access onto the A4142 Eastern By-pass.
                                </a>









                                <p class="address">


                                    Brasenose Farm Eastern By-Pass Road Oxford OX4 2QZ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03391/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 20 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 04 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P196JWMFFUL00&amp;activeTab=summary">
                                    Change of use of dwellinghouse (Use Class C3) to a House in Multiple Occupation (Use Class C4). Provision of bin and cycle stores. (Retrospective)
                                </a>









                                <p class="address">


                                    149 Reliance Way Oxford OX4 2FW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03381/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 20 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 05 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P17O9OMFFUA00&amp;activeTab=summary">
                                    Demolition of existing single storey side extension and erection of a two storey side extension to create 1 x 1-bed dwellinghouse (Use Class C3). Provision of private amenity space and bin and cycle storage.
                                </a>









                                <p class="address">


                                    44 Foxwell Drive Oxford OX3 9QE
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03374/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 19 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 05 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P17BR3MFFTN00&amp;activeTab=summary">
                                    Erection of first floor side and rear extension.
                                </a>









                                <p class="address">


                                    175 The Slade Oxford OX3 7HP
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03356/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 19 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 09 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P17BTVMFFU300&amp;activeTab=summary">
                                    Installation of a timber fence and gate to access on Collins Street. Alteration, replacement and insertion of additional windows and doors to all elevations at ground floor level. Alterations to existing single storey projection to south west elevation to add external insulation and render system, increase existing parapet height and add parapet to all edges. Provision of bin and cycle storage.
                                </a>









                                <p class="address">


                                    Hooper House 3 Collins Street Oxford OX4 1XS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03363/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 19 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 03 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P15TPOMFFT800&amp;activeTab=summary">
                                    Installation of kitchen extraction system, refurbish and re-paint shopfront, 4no. non-illuminated fascia signs, and internal shop fit-out in association with change of use from A1 to A5.
                                </a>









                                <p class="address">


                                    121 Covered Market Market Street Oxford OX1 3DZ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03347/LBC
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 18 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 02 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P0YF0SMFFRN00&amp;activeTab=summary">
                                    Erection of a rear garden outbuilding (Retrospective)
                                </a>









                                <p class="address">


                                    35 Phipps Road Oxford Oxfordshire OX4 3HJ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03316/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 14 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 02 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P0UF7UMF0XV00&amp;activeTab=summary">
                                    Erection of a two storey extension to create 2 x 1-bed flats (Use Class C3). Insertion of balcony and doors to side elevation. Provision of car parking and bin and cycle stores.
                                </a>









                                <p class="address">


                                    102 Spencer Crescent Oxford Oxfordshire OX4 4SW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03293/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 11 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 03 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P0OT4WMFFPL00&amp;activeTab=summary">
                                    Change of use from dwellinghouse (Use Class C3) to House in Multiple Occupation (Use Class C4).
                                </a>









                                <p class="address">


                                    5 Holyoake Road Oxford OX3 8AF
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03273/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Sat 09 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 05 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P0N9QHMF11T00&amp;activeTab=summary">
                                    Erection of single storey rear extension. Formation of new vehicle access from Banbury Road with provision of dropped kerb and car parking for 2No. vehicles (Amended Plans)
                                </a>









                                <p class="address">


                                    324 Banbury Road Oxford Oxfordshire OX2 7ED
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03262/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 07 Dec 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 02 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P0852FMFFKX00&amp;activeTab=summary">
                                    Provision of dropped kerb.
                                </a>









                                <p class="address">


                                    48 Bayswater Road Oxford OX3 9NY
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03172/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 30 Nov 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 10 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=P02XOXMFFJA00&amp;activeTab=summary">
                                    Continued use of land as car park for 12 months.
                                </a>









                                <p class="address">


                                    Car Park Osney Lane Oxford Oxfordshire
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/03140/FUL
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 27 Nov 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 30 Jan 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=OUCYALMFLUF00&amp;activeTab=summary">
                                    Variation of conditions 3 (Samples), condition 4 (Bike and bin stores) condition 7 (construction methods for foundations) and removal of condition 6 (Part M4(2)) of planning permission 15/03432/FUL (Demolition of existing house. Erection of 1 x 3-bed dwelling and 1 x 1-bed dwelling (Use Class C3). Provision of private amenity space, car parking and bin and cycle storage.(Amended plans)) to allow construction to begin before conditions 3,4, and 7 are met and to complete the application without gaining approval from the local authority regarding Part M4(2)
                                </a>









                                <p class="address">


                                    70 Glebelands Oxford OX3 7EN
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    17/02068/VAR
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 08 Aug 2017
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 22 Mar 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>


                        </ul>


                    </div>







                    <div class="clear"></div>

                    <p class="pager bottom"><span class="showing"><strong>Showing 801-816</strong> of 816</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=8" class="previous"><img border="0" src="http://public.oxford.gov.uk/online-applications-skin/images/pager_previous.gif" alt="" title="Display the previous page of search results"/>Previous</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=1" class="page">1</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=2" class="page">2</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=3" class="page">3</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=4" class="page">4</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=5" class="page">5</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=6" class="page">6</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=7" class="page">7</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=8" class="page">8</a><span class="divider">|</span><strong>9</strong><span class="divider">|</span></p>

                    <div class="clear"></div>
                </div>

                <!-- #EndEditable -->
                <!-- #BeginEditable "pagefoot" --><!-- #EndEditable -->
            </div>
            <!-- END WAM CONTENT -->

            <p id="poweredBy"><a href="http://www.idoxgroup.com/" target="_blank" title="This link will open a new window"><img src="http://public.oxford.gov.uk/online-applications-skin/images/idox.gif" alt="an Idox solution" /></a></p>
        </div>
        <div id="footer">
            <div class="container">
                <div id="links">
                    <ul>
                        <li><a href="https://www.oxford.gov.uk/contact">Contact Us</a></li><li><a href="https://www.oxford.gov.uk/cookies">Cookie Policy</a></li><li><a href="https://www.oxford.gov.uk/terms">Terms &amp; Conditions</a></li>
                    </ul>
                </div>
                <div id="footercontent"><p>Copyright &copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> Oxford City Council, Town Hall, St Aldate's, Oxford, OX1 1BX</p></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<!-- IDOX PA END -->

</body>
</html>

